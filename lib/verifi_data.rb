require "verifi_data/version"

module VerifiData

  class SingleAddress

    attr_reader :errors, :response, :street, :city, :state, :zip, :detail

    def initialize(street: street_in, city: city_in, state: state_in, zip: zip_in, detail: detail_in)
      @street = street
      @city = city
      @state = state
      @zip = zip 
      @detail = detail
      @errors = ActiveModel::Errors.new(self)
      @response = {}

    end


    def validate
      if api_key.blank?
        errors[:base] << "No API Key Found"
        return {errors: "No API Key Found"}
      end

      url = URI.encode(get_url)

      RestClient.get(url){ |response, request, result, &block|
        @code = response.code
        case response.code
        when 200
          @response = JSON.parse(response)
          #ap @response[0]["components"]
        else
          
          Rails.logger.info {"Error in #{self.class.name} #{@code}"}
          Rails.logger.info { response  }
          return response

        end
      }    

      return response

    end
    
    def get_url
      "http://api.verifi.io/v1/validate/address?api_key=#{api_key}&street=#{street}&city=#{city}&state=#{state}&zip=#{zip}&detail=#{detail}"
    end

    def api_key
      ENV["VERIFI_DATA_API_KEY"]
    end

    def read_attribute_for_validation(attr)
      send(attr)
    end

    def self.human_attribute_name(attr, options = {})
      attr
    end

    def self.lookup_ancestors
      [self]
    end


  end


end

